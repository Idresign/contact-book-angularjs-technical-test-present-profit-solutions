/*********************************
 * @ Author: Idriss Said Alaoui
 * @ Company: present profit solutions
 * @ Project: Contact Book - AngularJs Technical Test
 * @ Date: 2014-10-20
 * @ Description: contain the main logic (controller and model) of the application
 *********************************/

var app = angular.module('contactBook', ['page-components']);

app.controller('ContactCtrl', function($scope, $http) {

    // JSON results would be stored in the following Array
    $scope.contacts = [];

    // when the program is starting, it would be sorted in names, from A to Z
    $scope.orderByField = 'name.v';
  	$scope.reverseSort = false;

    // the following function upload the result of JSON to the array contacts;
    $scope.contactList = function() {
        var httpRequest = $http({
            url: 'https://script.googleusercontent.com/macros/echo?user_content_key=ydqmDwLvKqmwAsRHAuJnBT4NhHyfGOfPgKo4-vWuYXc16JTEAp5qvf2a8Tvp5qLFmI7P3IIl3M16cC6s7lXJeNqLYM69u914m5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnJhThdRmmV0UKNejsqmLoF_wQ4g2wbhDH-r2C96ThmctnmIXrmGu8rjRI3RO6mE7ymlvK7v7zPWZ&lib=MITcNQ0mkiZQZRdAvODvNJOuyEkHUnIBb'
        }).success(function(data, status) {
            $scope.contacts = data.values;
        });
    };

    // checks whether the birthday is less than 30 days, so to paint the corresponding row in yellow 
    $scope.checkYellow = function(birthday){
    	var diffDays = diffDate(birthday);
		return diffDays < 30 && diffDays >= 15;
	};

    // checks whether the birthday is less than 15 days, so to paint the corresponding row in red     
	$scope.checkRed = function(birthday){
		var diffDays = diffDate(birthday);
		return diffDays < 15 && diffDays >= 0;
	};

    // compute the difference in days between today and a specific birthday
    function diffDate(birthday){
        var date = new Date(birthday);
        var today = new Date();

        today.setFullYear(0);
        date.setFullYear(0);

        var ageDifMs = date.getTime() - today.getTime() ;
        var diffDays = ageDifMs - today.getFullYear() - date.getFullYear();
        
        return diffDays/(24*60*60*1000);
    }
});