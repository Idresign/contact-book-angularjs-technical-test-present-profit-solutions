/*********************************
 * @ Author: Idriss Said Alaoui
 * @ Company: present profit solutions
 * @ Project: Contact Book - AngularJs Technical Test
 * @ Date: 2014-10-20
 * @ Description: contain the logic of filters for processing information to display
 *********************************/

// the following filter transforms the date from JSON to display the age and birthdate
app.filter('ageFilter', function() {
     function calculateAge(birthday) { // birthday is a date
        var date = new Date(birthday);
		var ageDifMs = Date.now() - date.getTime();
		var ageDate = new Date(ageDifMs); // miliseconds from epoch
		return Math.abs(ageDate.getUTCFullYear() - 1970) + " (" + date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() +")";
     }

     return function(birthdate) { 
           return calculateAge(birthdate);
     }; 
});