/*********************************
 * @ Author: Idriss Said Alaoui
 * @ Company: present profit solutions
 * @ Project: Contact Book - AngularJs Technical Test
 * @ Date: 2014-10-20
 * @ Description: contain a mapping to html components
 *********************************/

(function(){
	var app = angular.module('page-components', [ ]);

	app.directive('pageLoader', function(){
	    return{
	        restrict: 'E',
	        templateUrl: 'loader.html'
	    };
	});

	app.directive('pageList', function(){
	    return{
	        restrict: 'E',
	        templateUrl: 'list.html'
	    };
	});
})();